#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>


#include "mem.h"



//printf ("%"PRIu64" - size header\n", offsetof(struct block_header, contents));
typedef void test (void* );


void test_1 (void* );
void test_2 (void* );
void test_3 (void* );
void test_4 (void* );
void test_5 (void* );

test* const tests[]  = {
        [0] = test_1,
        [1] = test_2,
        [2] = test_3,
        [3] = test_4,
        [4] = test_5,
};


int main(){


    void* heap = heap_init(8000);
    debug_heap(stdout, heap);

    for (size_t i = 0; i < 5; i++) {
        tests[i] (heap);
    }
    //tests[4](heap);
    return 0;
}

void* map_mem(void*, size_t);

void test_1 (void* heap) { //Обычное успешное выделение памяти
    printf ("-------TEST_1------\n");
    void* a = _malloc(25);
    debug_heap(stdout, heap);
    _free(a);
    printf("\n");
}

void test_2 (void* heap) { //Освобождение одного блока из нескольких выделенных.
    printf ("-------TEST_2------\n");
    void* a = _malloc(56);
    void* b = _malloc(20);
    _free(a);
    debug_heap(stdout, heap);
    _free(b);
    printf("\n");
}

void test_3 (void* heap) { //Освобождение двух блоков из нескольких выделенных.
    printf ("-------TEST_3------\n");
    void* a = _malloc(50);
    void* b = _malloc(40);
    void* c = _malloc(60);
    void* d = _malloc(80);
    printf("Before FREE:\n");
    debug_heap(stdout, heap);
    _free(b);
    _free(a);
    printf("After FREE:\n");
    debug_heap(stdout, heap);

    _free(c);
    _free(d);
    printf("\n");
}

void test_4 (void* heap) { //Память закончилась, новый регион памяти расширяет старый.

    printf ("-------TEST_4------\n");
    void* a = _malloc(13000);
    debug_heap(stdout, heap);
    printf ("\nFREE all blocks:\n");
    _free(a);
    debug_heap(stdout, heap);
    printf("\n");
}

void test_5 (void* heap) { // 5 test :)
    printf ("-------TEST_5------\n");
    printf("\nDefault heap = 6 pages\n");
    debug_heap(stdout, heap);


    printf("\nCreate new heap = 6 pages\n");
    void* heap_2 = map_mem(((uint8_t*)HEAP_START + getpagesize()*6),5000);
    fprintf(stderr, "Address HEAP_2 = %p\n", heap_2);
    debug_heap(stdout, heap);

    printf("\nCreate new block = 7 pages\n");
    void* a = _malloc(getpagesize()*7);
    debug_heap(stdout, heap);

    printf("\nFREE all blocks\n");
    _free(a);
    debug_heap(stdout, heap);

    printf("\n");
}

